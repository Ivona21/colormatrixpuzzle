﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ColorMatrixPuzzle;
using System.Collections.Generic;

namespace ColorMatrixTests
{
    [TestClass]
    public class ColorMatrixUnitTests
    {
        [TestMethod]
        //happy case - valid - same order
        public void IsRowValidTest1()
        {
            //arrange
            ColorMatrix.rules = new List<int>() { 1, 1, 2, 2, 2 };
            int[] row = new int[] { -1, 1, -1, -1, 1, 1, -1, -1 };

            //act
            bool isValid = ColorMatrix.IsRowValid(row);

            //assert
            Assert.AreEqual(true, isValid);
        }

        [TestMethod]
        //valid - different order
        public void IsRowValidTest2()
        {
            //arrange
            ColorMatrix.rules = new List<int>() { 1, 1, 2, 2, 2 };
            int[] row = new int[] { -1, -1, 1, -1, 1, 1, -1, -1 };

            //act
            bool isValid = ColorMatrix.IsRowValid(row);

            //assert
            Assert.AreEqual(true, isValid);
        }

        [TestMethod]
        //invalid
        public void IsRowValidTest3()
        {
            //arrange
            ColorMatrix.rules = new List<int>() { 1, 1, 2, 2, 2 };
            int[] row = new int[] { 1, 1, 1, 1, 1, 1, 1, 1 };

            //act
            bool isValid = ColorMatrix.IsRowValid(row);

            //assert
            Assert.AreEqual(false, isValid);
        }

        [TestMethod]
        //invalid
        public void IsRowValidTest4()
        {
            //arrange
            ColorMatrix.rules = new List<int>() { 1, 1, 2, 2, 2 };
            int[] row = new int[] { -1, -1, 1, -1, -1, -1, -1, -1 };

            //act
            bool isValid = ColorMatrix.IsRowValid(row);

            //assert
            Assert.AreEqual(false, isValid);
        }

        [TestMethod]
        //invalid
        public void IsRowValidTest5()
        {
            //arrange
            ColorMatrix.rules = new List<int>() { 1, 1, 2, 2, 2 };
            int[] row = new int[] { -1, 1, 1, 1, -1, 1, -1, 1 };

            //act
            bool isValid = ColorMatrix.IsRowValid(row);

            //assert
            Assert.AreEqual(false, isValid);
        }

        [TestMethod]
        //valid - different values
        public void IsRowValidTest6()
        {
            //arrange
            ColorMatrix.rules = new List<int>() { 2, 2, 4 };
            int[] row = new int[] { -1, -1, 1, 1, 1, 1, -1, -1 };

            //act
            bool isValid = ColorMatrix.IsRowValid(row);

            //assert
            Assert.AreEqual(true, isValid);
        }

        [TestMethod]
        //invalid - different values -4/4
        public void IsRowValidTest7()
        {
            //arrange
            ColorMatrix.rules = new List<int>() { 2, 2, 4 };
            int[] row = new int[] { -1, -1, -1, -1, 1, 1, 1, 1 };

            //act
            bool isValid = ColorMatrix.IsRowValid(row);

            //assert
            Assert.AreEqual(false, isValid);
        }

        [TestMethod]
        //invalid - different values - 8
        public void IsRowValidTest8()
        {
            //arrange
            ColorMatrix.rules = new List<int>() { 2, 2, 4 };
            int[] row = new int[] { 1, 1, 1, 1, 1, 1, 1, 1 };

            //act
            bool isValid = ColorMatrix.IsRowValid(row);

            //assert
            Assert.AreEqual(false, isValid);
        }

        [TestMethod]      
        public void IsRowValidTest9()
        {
            //arrange
            ColorMatrix.rules = new List<int>() { 4 };
            int[] row = new int[] { 1, 1, 1, 1 };

            //act
            bool isValid = ColorMatrix.IsRowValid(row);

            //assert
            Assert.AreEqual(true, isValid);
        }

        [TestMethod]       
        public void IsRowValidTest10()
        {
            //arrange
            ColorMatrix.rules = new List<int>() { 4 };
            int[] row = new int[] { 1, 1, -1, 1 };

            //act
            bool isValid = ColorMatrix.IsRowValid(row);

            //assert
            Assert.AreEqual(false, isValid);
        }

        [TestMethod]
        public void IsRowValidTest11()
        {
            //arrange
            ColorMatrix.rules = new List<int>() { 3, 1};
            int[] row = new int[] { 1, 1, 1, -1 };

            //act
            bool isValid = ColorMatrix.IsRowValid(row);

            //assert
            Assert.AreEqual(true, isValid);
        }

        [TestMethod]
        public void IsRowValidTest12()
        {
            //arrange
            ColorMatrix.rules = new List<int>() { 3, 1 };
            int[] row = new int[] { -1, 1, 1, -1 };

            //act
            bool isValid = ColorMatrix.IsRowValid(row);

            //assert
            Assert.AreEqual(false, isValid);
        }

        [TestMethod]
        public void IsRowValidTest13()
        {
            //arrange
            ColorMatrix.rules = new List<int>() { 3 };
            int[] row = new int[] { -1, -1, -1 };

            //act
            bool isValid = ColorMatrix.IsRowValid(row);

            //assert
            Assert.AreEqual(true, isValid);
        }

        [TestMethod]
        public void IsRowValidTest14()
        {
            //arrange
            ColorMatrix.rules = new List<int>() { 3 };
            int[] row = new int[] { -1, -1, 1 };

            //act
            bool isValid = ColorMatrix.IsRowValid(row);

            //assert
            Assert.AreEqual(false, isValid);
        }

        [TestMethod]
        public void IsRowValidTest15()
        {
            //arrange
            ColorMatrix.rules = new List<int>() { 1, 1, 2, 2, 2 };
            int[] row = new int[] { 1, 1, -1, -1, -1, 1, -1, 1 };

            //act
            bool isValid = ColorMatrix.IsRowValid(row);

            //assert
            Assert.AreEqual(false, isValid);
        }

        [TestMethod]
        public void AreAllColumnsValidTest()
        {
            ColorMatrix.columnRules = new List<int>[] {
                new List<int>() {4},
                new List<int>() {4 },
                new List<int>() {4 },
                new List<int>() {4 },
                new List<int>() {4},
                new List<int>() {1, 3 },
                new List<int>() {1, 1, 2 },
                new List<int>() { 1, 1, 2 }
            };

            int[] arr = new int[] { 1, 1, -1, -1, 1, -1, -1, 1,
                                    1, 1, -1, -1, -1, -1, 1, 1,
                                    1, 1, -1, -1, -1, 1, -1, -1,
                                    1, 1, -1, -1, 1, 1, -1, 1};

            bool areValid = ColorMatrix.AreAllColumnsValid(arr, 8, 4);

            Assert.AreEqual(false, areValid);
        }

        [TestMethod]
        public void AreAllColumnsValidTest2()
        {
            ColorMatrix.columnRules = new List<int>[] {
                new List<int>() {4},
                new List<int>() {4 },
                new List<int>() {4 },
                new List<int>() {4 },
                new List<int>() {4},
                new List<int>() {1, 3 },
                new List<int>() {1, 1, 2 },
                new List<int>() { 1, 1, 2 }
            };

            int[] arr = new int[] { 1, 1, -1, -1, 1, 1, -1, 1,
                                    1, 1, -1, -1, 1, 1, 1, 1,
                                    1, 1, -1, -1, 1, 1, 1, -1,
                                    1, 1, -1, -1, 1, -1, -1, 1};

            bool areValid = ColorMatrix.AreAllColumnsValid(arr, 8, 4);

            Assert.AreEqual(true, areValid);
        }
    }
}
