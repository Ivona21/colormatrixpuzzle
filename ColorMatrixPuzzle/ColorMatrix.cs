﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorMatrixPuzzle
{
    public static class ColorMatrix
    {
        public static List<int> rules = new List<int>();
        public static List<int>[] rowsRules;
        public static List<int>[] columnRules;

        public static bool IsRowValid(int[] row)
        {
            List<int> code = new List<int>();
            int counter = 1;
            for (int i = 0; i < row.Length; i++)
            {
                int previous = 0;
                if (i > 0)
                {
                    previous = row[i - 1];
                }
                int current = row[i];

                int next = 0;
                if (i != row.Length - 1)
                {
                    next = row[i + 1];
                }

                if (previous == current)
                {
                    counter++;
                }

                if (current != next)
                {
                    code.Add(counter);
                    counter = 1;
                }
            }
            bool areSame = rules.Count == code.Count && rules.All(code.Contains) && code.All(rules.Contains);
            return areSame;
        }

        public static bool AreAllRowsValid(int [] arr, int columns, int rows)
        {
            for (int i = 0; i < rows; i++)
            {                
                int[] rowToTest = arr.Skip(i * columns).Take(columns).ToArray();
                ColorMatrix.rules = ColorMatrix.rowsRules[i];
                bool valid = IsRowValid(rowToTest);
                if (!valid)
                {
                    return false;
                }
            }
            return true;            
        }

        public static bool AreAllColumnsValid(int [] arr, int columns, int rows)
        {
            int[][] columnsToTest = new int[columns][];
            for (int i = 0; i < columns; i++)
            {
                columnsToTest[i] = new int[rows];
            }

            int counter = 0;
            for (int i = 0; i < columns; i++)
            {
                for (int j = 0, c = 0; j < rows; j++, c+=columns)
                {
                    columnsToTest[i][j] = arr[c + i];
                }
                int[] columnToTest = columnsToTest[counter++];
                ColorMatrix.rules = ColorMatrix.columnRules[counter-1];
                bool columnValid = IsRowValid(columnToTest);
                if (!columnValid)
                {
                    return false;
                }              
            }
            return true;
        }

        public static void Print2DFrom1DArray(int [] arr, int columns, int rows)
        {            
            int[,] arr2d = new int[columns, rows];

            int counter = 0;
            for (int row = 0; row < rows; row++)
            {
                for (int column = 0; column < columns; column++)
                {
                    arr2d[column, row] = arr[counter++];
                    Console.Write(arr2d[column, row] == 1 ? " 0 " : " 1 "); 
                }
                Console.WriteLine();
            }
            Console.WriteLine("=================================");     
        }

        public static void ColorArray(int[] arr, int index, int columns, int rows)
        {
            if (index % columns == 0 && index != 0)
            {
                for (int i = 0; i < columns; i++)
                {
                    int[] rowToCheck = arr.Skip(index - columns).Take(columns).ToArray();

                    ColorMatrix.rules = rowsRules[index / columns - 1];
                    bool isRowValid = IsRowValid(rowToCheck);
                    if (isRowValid == false)
                    {
                        return;
                    }
                    else if (isRowValid == true)
                    {
                        int a = index;
                    }
                }
            }

            if (index == arr.Length - 1 && !arr.Contains(0))
            {
                bool allRowsValid = AreAllRowsValid(arr, columns, rows);
                if (allRowsValid == false)
                {
                    return;
                }
                bool allColumnsValid = AreAllColumnsValid(arr, columns, rows);
                if (allColumnsValid == false)
                {
                    return;
                }

                Print2DFrom1DArray(arr, columns, rows);               
                return;               
            }

            arr[index] = -1;
            ColorArray(arr, index + 1, columns, rows);

            arr[index] = 1;
            ColorArray(arr, index + 1, columns, rows);
        }
    }
}
