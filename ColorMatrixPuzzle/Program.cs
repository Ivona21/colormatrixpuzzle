﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorMatrixPuzzle
{
    class Program
    {
        static void Main(string[] args)
        {
            //small matrix
            ColorMatrix.rowsRules = new List<int>[]
            {
                new List<int>() {1, 2 },
                new List<int>() {1, 2 },
                new List<int>() {1, 1, 1 }
            };

            ColorMatrix.columnRules = new List<int>[] {
                new List<int>() {1, 1, 1 },
                new List<int>() {3 },
                new List<int>() {1, 2 }
            };
            ColorMatrix.ColorArray(new int[9], 0, 3, 3);
           
            //big matrix
            ColorMatrix.rowsRules = new List<int>[]
           {
                new List<int>() {1, 1, 2, 2, 2 },
                new List<int>() {2, 2, 4 },
                new List<int>() {1, 2, 2, 3 },
                new List<int>() { 1, 1, 2, 2, 2 }
           };

            ColorMatrix.columnRules = new List<int>[] {
                new List<int>() {4},
                new List<int>() {4 },
                new List<int>() {4 },
                new List<int>() {4 },
                new List<int>() {4},
                new List<int>() {1, 3 },
                new List<int>() {1, 1, 2 },
                new List<int>() { 1, 1, 2 }
            };

            ColorMatrix.ColorArray(new int[32], 0, 8, 4);
            Console.ReadLine();
        }
    }
}
